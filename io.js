const linker = require('./linker')

module.exports = server => {
    const io = require('socket.io')(server);

    io.on('connection', listener)
}

function listener(socket) {
    console.log('Ha habido una nueva conexión');
    linker.on('message', message => {
        console.log('nuevo mensaje detectado');
        socket.emit('new-message', message)
    })

    setTimeout(() => {
        socket.emit('all-messages', { messages: linker.messages })
    }, 200);
}