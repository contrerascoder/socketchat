const http = require('http');
const linker = require('./linker')
const fs = require('fs')
const PORT = process.env.PORT || 8080

const server = http.createServer(require('./app'));
require('./io')(server)

server.listen(PORT, function() {
    console.log(`Escuchando por el puertto ${PORT}`);
})

process.on('SIGINT', () => {
    fs.writeFileSync('./messages.json', JSON.stringify(linker.messages))
    process.exit(0)
})