const { EventEmitter } = require('events')

class Linker extends EventEmitter {
    constructor() {
        super()
        this.on('push-message', this.pushMessage)
        try {
            this.messages = require('./messages.json')
            console.log(this.messages);
        } catch (error) {
            this.messages = []
            console.log('no hay archivo para cargar');
        }
    }

    pushMessage(message) {
        console.log('Insertando un nuevo mensaje');
        this.messages.push(message)
        this.emit('message', message)
    }
}

module.exports = new Linker()