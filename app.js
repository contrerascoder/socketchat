const express = require('express')
const linker = require('./linker')
const path = require('path')

const app = express()
app.engine('pug', require('pug').__express)
app.set('view engine', 'pug');
app.use(express.json())
app.use(express.static(path.join(__dirname, 'public')))

app.get('/', (req, res) => {
    res.render('index')
})

app.post('/mensaje', (req, res) => {
    linker.emit('push-message', req.body.mensaje)
    res.end('Todo ok')
})

module.exports = app