# Chat con socket.io
 * El archivo principal es [index](index.js), en el se crea el servidor http
 * En el archivo [io](io.js) se configura todo lo relaccionado con `socket.io`
 * En el archivo [app](app.js) se configura todo lo relacionado con la aplicación express
 * El fichero [linker](linker.js) sirve de enlace entre los web sockets y la aplicacion express

> Cada vez que se manda un mensaje, se accede a emitir a linker un evento `push-message`, en ese evento se mete el mensaje en el array volatil de mensajes. Despues linker, emite de si mismo un evento message el cual lo recibe socket.io, el cual al momento de recibir cada conexión de cada socket, se ha suscrito al evento message de modo que puede mandar el mensaje a el cliente, para que este lo inserte.

> Para que los mensajes al cerrar el servidor no se borren, al cerrar el servidor, se escriben en un archivo json. Para ello, he escuchado el evento `SIGINT` de process para de esa forma, de manera sincrona, escribir en el archivo `messages.json` todo el contenido del array messages de linker.js. Al momemnto de cargar el programa, ya que el archivo messages.json podría no existir, se hace un try catch para comprobar que, en caso de que no exista, se inicialize a un array vacio

Hecho por Raul Contreras