var socket = io();
var loaded = false

window.onload = () => {
    const send = document.getElementById('send')
    const messageInput = document.getElementById('message')

    send.onclick = sendMessage
    messageInput.onkeydown = e => {
        if (e.key === 'Enter') sendMessage()
    }

    function sendMessage() {
        var mensaje = messageInput.value
        axios.post('/mensaje', { mensaje: mensaje })
            .then(() => {
                console.log('mesnaje enviado: ' + mensaje)
                messageInput.value = ""
                window.scrollTo(0, document.body.scrollHeight);
            })
            .catch(console.log)
    }

    socket.on('new-message', message => {
        console.log('nuevo mensaje recibido');
        document.getElementById('messages').insertAdjacentHTML('beforeend', `<li>${message}</li>`)
    })
    socket.on('all-messages', ({ messages }) => {
        if (loaded) {
            console.log('mensajes cargados');
            return
        }

        for (const message of messages)
            document.getElementById('messages').insertAdjacentHTML('beforeend', `<li>${message}</li>`)

        loaded = true;
        window.scrollTo(0, document.body.scrollHeight);
    })
}